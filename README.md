# High level goals:

* Collaborate with colleagues to produce documentation that will:

    1) Elaborate on https://gitlab.com/gitlab-org/gitlab/-/issues/332951 explaining pitfalls

    2) Produce documentation to send to the customer for https://gitlab.zendesk.com/agent/tickets/252646

    3) Produce documentation for the support team and customers for ticket deflection
